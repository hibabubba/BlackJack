package com.example;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

import java.util.ArrayList;
import java.util.Collections;

public class Hentkortstokk { //TEST

    public ArrayList<String> kortstokkNy= new ArrayList<String>();
    public void shuffle(String svar ,int lengde){

        String[] kort = svar.split("[{}]");
        int i=1;
        while(i < lengde){
            kortstokkNy.add(kort[i]);
            i= i+2;
        }
         //shuffle den kortstokken
        
      Collections.shuffle(kortstokkNy);
    

    }
    public ArrayList<String> hentKortstokkNy(){
      return kortstokkNy;
    }
  
    Hentkortstokk(URL deck) {
      String a = null;

      try {
        //"http://nav-deckofcards.herokuapp.com/shuffle" Brukte den når jeg testet klassen
        
        HttpURLConnection con = (HttpURLConnection) deck.openConnection();
        con.setRequestMethod("GET");
        int status = con.getResponseCode();
        //hvis requesten failer
        Reader streamReader = null;
   
        if (status > 299) {
            
            streamReader = new InputStreamReader(con.getErrorStream());
        
           
        } else {
            streamReader = new InputStreamReader(con.getInputStream());
        }
      //hvis vi får response

            StringBuffer content = new StringBuffer();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
               
            String svar;      
            while ((svar = in.readLine()) != null) {
                content.append(svar);
            }
            in.close();

            con.disconnect();
            a = content.toString();

          this.shuffle(a, (52*2)+1); //kortstokk er alltid 52
            

          }
          catch(IOException e) {
            e.printStackTrace();
            System.out.println("noe er feil");
          }



      
    
    }

  }
  