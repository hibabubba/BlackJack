package com.example;

import com.example.Spiller;

import java.util.*;

public class StartSpill{

    
   

    public int finntall(String tall){

        if(tall.equals("K") || tall.equals("J") || tall.equals("Q")){
            return 10;
        }
        else if(tall.equals("A")){
            return 11;
        }
        else{
            return Integer.parseInt(tall);
        }
    }
    public String finnforbokstav(String suit){
        if(suit.equals("DIAMONDS")){
            return "D";
        }else if(suit.equals("CLUBS")){
            return "C";
        }else if(suit.equals("HEARTS")){
            return "H";
        }else{
            return "S";
        }
    }

    public void settInn( ArrayList<String> kortstokk, Spiller p){
        String a = kortstokk.get(0);
        String[] kort = a.split("[:,]");
        p.suit.add(kort[1]);
        p.value.add(kort[3]);
        String tall = kort[3].replaceAll("^[\"']+|[\"']+$", ""); // " "
        System.out.println( " TALL: " + tall);
        int verdi = finntall(tall);
        p.regnUt(verdi);
        kortstokk.remove(0);
    }
    public void resultat(Spiller p){
        System.out.println("\n Navn "+ p.navn );
        System.out.println(" Totalsum : "+ p.totalsum);
        System.out.print(" Kort " + p.navn +" har : ");

        
        for(int i = 0; i< p.suit.size();i++){
            String suit = p.suit.get(i).replaceAll("^[\"']+|[\"']+$", "");
            String value = p.value.get(i).replaceAll("^[\"']+|[\"']+$", "");
            String forbokstav = finnforbokstav(suit);
            String slutt = forbokstav.concat(value +"|");
            System.out.print(slutt);
        }

    }

    public int spill(Spiller m, Spiller s, ArrayList<String> kortstokk){
        int svar =-1;
        //ta ut de 4 første kortene
        int i=0 ;
        while(i<4){
            if(i<2){
                settInn(kortstokk,s);
            }else{
                settInn(kortstokk,m);
            }
            i++;
        }
        System.out.println("ETTER FØRSTE RUNDE SITTER VI IGJEN MED: \n" );
        System.out.println("Magnus: " + m.totalsum);
        System.out.println("Du: " + s.totalsum);
        //sjekker vi første
        System.out.println("ANDRE RUNDE: \n" );
        if(m.totalsum == 21 && s.totalsum== 21){

            System.out.println(" Begge hadde 21, Dette betyr ingen vinner. Lykke til neste gang!\n");
            return 0;
        }
        if(m.totalsum == 21){
            System.out.println(" Gratulerer til Magnus, han vant! bedre lykke neste gang :)\n");
            return 5;

        }if(s.totalsum == 21){
            System.out.println(" Gratulerer til DEG, du vant!\n");
            return 10;
        }
         //spiller trekker til enten over 17 eller mindre enn 21

            while(s.totalsum < 17 && s.totalsum < 21){
                settInn(kortstokk,s);

            } //sjekke for om den er over 21
            if(s.totalsum > 21){
                System.out.println(" Gratulerer til Magnus, han vant! bedre lykke neste gang :)\n");
               return 6;
            }

            //MAGNUS SIN TUR
            while(m.totalsum < s.totalsum && m.totalsum < 21){
                settInn(kortstokk,m);

            }
            if(m.totalsum > 21){//sjekke for om den er over 21
                System.out.println(" Gratulerer til DEG, du vant!\n");
                return 11;
            }
            else{
                System.out.println(" Gratulerer til Magnus, han vant! bedre lykke neste gang :)\n");
                return 7;
            }


        //return svar;
    }

   StartSpill(Spiller m, Spiller s, ArrayList<String> kortstokk) {

        spill(m, s, kortstokk);
        System.out.println(" ----RESULTAT----");
        resultat(m);
        resultat(s);
        System.out.println(" \n");


    }
}
        