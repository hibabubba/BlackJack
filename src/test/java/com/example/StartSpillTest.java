package com.example;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StartSpillTest {



    public ArrayList<String> TestArray(String en, String to, String tre, String fire,String fem,String seks){
        ArrayList<String> kortstokk = new ArrayList<String>();
        kortstokk.add(en);
        kortstokk.add(to);
        kortstokk.add(tre);
        kortstokk.add(fire);
        kortstokk.add(fem);
        kortstokk.add(seks);
        return kortstokk;
    }
    //LAGE ALLE OBJEKTER OG ARRAYER JEG TRENGER
    Spiller m = new Spiller("Magnus");
    Spiller s = new Spiller("MEG");
    ArrayList<String> kortstokk = TestArray("suit:HEARTS,value:K","suit:CLUBS,value:A",
            "suit:SPADES,value:K","suit:DIAMONDS,value:A",
            "suit:SPADES,value:A","suit:DIAMONDS,value:A");
    ArrayList<String> kortstokk21 = TestArray("suit:HEARTS,value:K","suit:CLUBS,value:A",
            "suit:SPADES,value:K","suit:DIAMONDS,value:A",
            "suit:SPADES,value:A","suit:DIAMONDS,value:A");

    ArrayList<String> kortstokkm21 = TestArray("suit:HEARTS,value:9","suit:CLUBS,value:A",
            "suit:SPADES,value:K","suit:DIAMONDS,value:A",
            "suit:SPADES,value:A","suit:DIAMONDS,value:A");

    ArrayList<String> kortstokks21 = TestArray("suit:HEARTS,value:K","suit:CLUBS,value:A",
            "suit:SPADES,value:9","suit:DIAMONDS,value:A",
            "suit:SPADES,value:A","suit:DIAMONDS,value:A");

    ArrayList<String> kortstokkm = TestArray("suit:HEARTS,value:9","suit:CLUBS,value:9",
            "suit:SPADES,value:5","suit:DIAMONDS,value:6",
            "suit:SPADES,value:A","suit:DIAMONDS,value:A");
    ArrayList<String> kortstokks = TestArray("suit:HEARTS,value:5","suit:CLUBS,value:5",
            "suit:SPADES,value:6","suit:DIAMONDS,value:6",
            "suit:SPADES,value:K","suit:DIAMONDS,value:A");
    ArrayList<String> kortstokkh = TestArray("suit:HEARTS,value:6","suit:CLUBS,value:6",
            "suit:SPADES,value:6","suit:DIAMONDS,value:6",
            "suit:SPADES,value:K","suit:DIAMONDS,value:A");
    StartSpill spill = new StartSpill(m, s, kortstokk );

    @Test
    public void TestStartSpill(){


        //klassen
        m.totalsum = 0; s.totalsum = 0; m.suit.clear(); m.value.clear(); s.suit.clear(); s.value.clear();
        assertEquals(0,spill.spill(m,s,kortstokk21), " Begge får 21 det vil si int i return er 0 og begge taper " );
        m.totalsum =0; s.totalsum =0; m.value.clear(); s.suit.clear(); s.value.clear();
        assertEquals(5,spill.spill(m,s,kortstokkm21), " Magnus får 21 det vil si int i return er 5 og magnus vinner" );
        m.totalsum =0; s.totalsum =0; m.value.clear(); s.suit.clear(); s.value.clear();
        assertEquals(10,spill.spill(m,s,kortstokks21), " Du får 21 det vil si int i return er 10 og du vinner " );
        m.totalsum =0; s.totalsum =0; m.value.clear(); s.suit.clear(); s.value.clear();
        assertEquals(11,spill.spill(m,s,kortstokkm), " Du trakk for mye, dvs return int er 6 og magnus vinner " );
        m.totalsum =0; s.totalsum =0; m.value.clear(); s.suit.clear(); s.value.clear();
        assertEquals(11,spill.spill(m,s,kortstokks), " Mangus  trakk for mye, dvs return int er 11 og du vinner " );

        //Metode finntall
        assertEquals(5,spill.finntall("5"), " Kort 5 gir 5 " );
        assertEquals(10,spill.finntall("K"), " Kort K gir 10 " );
        assertEquals(11,spill.finntall("A"), " Kort A gir 11 " );

        //metode finnforbokstav
        assertEquals("C",spill.finnforbokstav("CLUBS"), " CLUBS gir C " );
        assertEquals("H",spill.finnforbokstav("HEARTS"), " HEARTS gir H " );
        assertEquals("D",spill.finnforbokstav("DIAMONDS"), " DIAMONDS gir D " );
        assertEquals("S",spill.finnforbokstav("SPADES"), " SPADES gir S " );

        //metode settInn
        m.totalsum = 0;  m.suit.clear(); m.value.clear();
        Spiller h = new Spiller("tull");
        StartSpill spill = new StartSpill(m, h, kortstokkh);

        assertEquals("HEARTS",h.suit.get(0), " suit HEARTS i kortstokken skal gi:  h.suit = HEARTS " );
        assertEquals("6",h.value.get(0), "value index 0 i kortstokken skal gi:  h.value = 5 " );
        assertEquals(22,h.totalsum, " value 20 i kortstokken skal gi:  h.totalsum = 20 " );
        assertEquals("suit:DIAMONDS,value:A",kortstokkh.get(0), " Fjerning av element HEARTS, skal gi element CLUBS i invex 0" );


    }




}
